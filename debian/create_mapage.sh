#!/bin/bash
# create manpage from sgml file, needs docbook-to-man package

MAN=bumprace
SECTION=6

docbook-to-man $MAN.sgml > $MAN.$SECTION
nroff -man $MAN.$SECTION | less
